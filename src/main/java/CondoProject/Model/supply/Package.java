package CondoProject.Model.supply;

import javafx.scene.control.CheckBox;

public class Package extends Letter {
    private String delivery;
    private String track;
    private CheckBox checkBox;



    public Package(String name,String surname,String sentName, String sentSurname, String address,String size,String delivery,String track,String picture,String day,String time) {
        super(name, surname, sentName, sentSurname, address, size, picture, day, time);
        this.delivery = delivery;
        this.track = track;
        this.checkBox = new CheckBox();


    }



    public String getTrack() {
        return track;
    }

    public String getDelivery() {
        return delivery;
    }


    public Package() {

    }

    public void setDelivery(String delivery) {
        this.delivery = delivery;
    }

    public void setTrack(String track) {
        this.track = track;
    }




    @Override
    public String toString() {
        return "Package{" +
                "sentName='" + super.getName() + '\'' +
                ", simpleDateFormat=" + super.getDay() +
                ", sentSurname='" + super.getSurname() + '\'' +
                ", delivery='" + delivery + '\'' +
                ", track='" + track + '\'' +
                ", address='" + super.getAddress() + '\'' +
                ", size=" + super.getSize() +
                ", picture='" + super.getPicture() + '\'' +
                '}';
    }
}
