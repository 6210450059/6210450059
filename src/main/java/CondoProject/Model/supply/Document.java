package CondoProject.Model.supply;

import javafx.scene.control.CheckBox;

public class Document extends Letter {
    private String priority;
    private CheckBox checkBox;


    public Document(String name,String surname,String sentName, String sentSurname, String address,String size,String Priority,String picture,String day,String time) {
        super(name, surname, sentName, sentSurname, address, size, picture, day, time);
        this.checkBox = new CheckBox();
        this.priority = Priority;



    }





    public void setPriority(String priority) {
        this.priority = priority;
    }




    public String getPriority() {
        return priority;
    }

    public Document() {
    }

    @Override
    public String toString() {
        return "Document{" +
                "simpleDateFormat=" + super.getDay() +
                ", sentName='" + super.getSentName() + '\'' +
                ", sentSurname='" + super.getSentSurname() + '\'' +
                ", address='" + super.getAddress() + '\'' +
                ", size=" + super.getSize() +
                ", picture='" + super.getPicture() + '\'' +
                ", priority='" + priority + '\'' +
                '}';
    }
}
