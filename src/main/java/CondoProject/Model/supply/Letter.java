package CondoProject.Model.supply;

import javafx.scene.control.CheckBox;
import CondoProject.Model.Condo;

public class Letter {
    private String name;
    private String surname;
    private String date;
    private String sentName;
    private String sentSurname;
    private Condo condo;
    private String address;
    private double size;
    private String picture;
    private String day;
    private String time;
    private CheckBox checkBox;
    public CheckBox getCheckBox() {
        return checkBox;
    }

    public void setCheckBox(CheckBox checkBox) {
        this.checkBox = checkBox;
    }
    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }


    public void setTime(String time) {
        this.time = time;
    }

    public String getTime() {
        return time;
    }



    public Letter(String name,String surname,String sentName, String sentSurname, String address,String size,String picture,String day,String time) {

        this.name = name;
        this.surname = surname;
        this.sentName = sentName;
        this.sentSurname = sentSurname;
        this.address = address;
        this.size = Double.parseDouble(size);
        this.picture = picture;
        this.day = day;
        this.time = time;
        this.checkBox = new CheckBox();


    }

    public void setCondo(Condo condo) {
        this.condo = condo;
    }

    public Condo getCondo() {
        return condo;
    }


    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }


    public void setSimpleDateFormat(String date) {
        this.date = date;
    }




    public String getSimpleDateFormat() {
        return date;
    }

    public String getSentName() {
        return sentName;
    }

    public void setSentName(String sentName) {
        this.sentName = sentName;
    }

    public String getSentSurname() {
        return sentSurname;
    }

    public void setSentSurname(String sentSurname) {
        this.sentSurname = sentSurname;
    }

    public Letter() {
        picture = new String();
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getSize() {
        return size;
    }

    public void setSize(double size) {
        this.size = size;
    }

    public String getPicture() {
        return picture.toString();
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    @Override
    public String toString() {
        return "Letter{" +
                "simpleDateFormat=" + date +
                ", sentName='" + sentName + '\'' +
                ", sentSurname='" + sentSurname + '\'' +
                ", address='" + address + '\'' +
                ", size=" + size +
                ", picture='" + picture + '\'' +
                '}';
    }
}

