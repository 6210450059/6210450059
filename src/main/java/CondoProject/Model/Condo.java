package CondoProject.Model;

import java.util.Comparator;

public class Condo {
    private String building;
    private String layers;
    private String room;
    private String roomType;

    public Condo(String building, String layers, String room,String roomType) {
        this.building = building;
        this.layers = layers;
        this.room = room;
        this.roomType = roomType;
    }
    public Condo(String layers, String room,String roomType) {
        this.layers = layers;
        this.room = room;
        this.roomType = roomType;
    }

    public Condo() {
    }


    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public String getLayers() {
        return layers;
    }

    public void setLayers(String layers) {
        this.layers = layers;
    }

    public String getRoom() {
        return room;
    }

    public void setRoomtype(String types) {
        this.roomType = types;
    }

    public String getRoomType() {
        return roomType;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    @Override
    public String toString() {
        return building+","+layers+","+room+","+roomType;
    }



}
