package CondoProject.Model.People;

public class Employee extends Customer implements Comparable<Employee>{
    private String time;
    private String day;

    public Employee() {
    }
    public Employee(String id,String pin) {
        super.setId(id);
        super.setPin(pin);
    }

    public Employee(String name, String surname, String id,String pin,String day,String time) {
        super(name, surname, id, pin);
        this.time = time;
        this.day = day;

    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    @Override
    public String toString() {
        return super.getName() + " " + super.getSurname();

    }

    @Override
    public int compareTo(Employee o) {
        if (time.compareTo(o.getTime()) < 0 && day.compareTo(o.getDay()) < 0 ) return  -1;
        if (time.compareTo(o.getTime()) > 0 && day.compareTo(o.getDay()) > 0 ) return  1;
        return 0;
    }
}


