package CondoProject.Model.People;


public class Customer{
    private String name;
    private String surname;
    private String pin;
    private String id;
    private String building;
    private String layer;
    private String room;
    private String roomType;

    public Customer(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }
    public Customer(){

    }
    public Customer(String name,String surname,String id,String pin){
        this.name = name;
        this.surname = surname;
        this.pin = pin;
        this.id = id;


    }


    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getPin() {
        return pin;
    }

    public String getId() {
        return id;
    }

    public Customer(String name, String surname, String id, String pin, String building, String layer, String room, String roomtype) {
        this.name = name;
        this.surname = surname;
        this.pin = pin;
        this.id = id;
        this.building = building;
        this.layer = layer;
        this.room = room;
        this.roomType = roomtype;




    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public void setLayer(String layer) {
        this.layer = layer;
    }

    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }

    public String getRoom() {
        return room;
    }

    public String getBuilding() {
        return building;
    }

    public String getLayer() {
        return layer;
    }

    public String getRoomType() {
        return roomType;
    }

    public void setRoom(String room) {
        this.room = room;
    }


    public void setId(String id) {
        this.id = id;
    }




    public void setName(String name) {
        this.name = name;
    }


    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
//                ", letter=" + letter +
//                ", document=" + document +
//                ", aPackage=" + aPackage +
                ", Building= " + building +", "+ "Layer: "+ layer + ", "+"Room: "+room+", RoomType: "+roomType + '\'' +
                ", pin='" + pin + '\'' +
                ", id='" + id + '\'' +
                '}';
    }


}
