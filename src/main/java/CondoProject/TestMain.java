package CondoProject;

import CondoProject.Model.People.Customer;
import CondoProject.Controller.ControllerClass;
import CondoProject.Data.DataB;
import CondoProject.Data.ReadWriteFile.DataEmployeeFileSource;

/*** Test ***/
public class TestMain {
    public static void main(String[] args) {
        DataB dataFile = new DataEmployeeFileSource("data", "employee.csv");
        ControllerClass control = new ControllerClass();
        control.setAccountList(dataFile.Read().toListEmployee());
        dataFile = new DataEmployeeFileSource("data", "customer.csv");
        control.addAccountList(dataFile.Read().toListCustomer());
        for (Customer c : control.toListEmployee()){
            System.out.println(c.toString());
        }

        }



    }
