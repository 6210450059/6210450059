package CondoProject.Services.EditSupply;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import CondoProject.Model.People.Customer;
import CondoProject.Model.supply.Document;
import CondoProject.Model.supply.Letter;
import CondoProject.Model.supply.Package;
import CondoProject.Controller.ControllerClass;
import CondoProject.Services.DisplayTable.LetterDisplay;

import java.io.IOException;

public class EditSupplyRegister {
    private ControllerClass control;
    private ControllerClass SupplyControllerClass;
    @FXML private Label label;
    @FXML private Button NBTN,BBTN,PICTURE;
    @FXML private CheckBox PackageCheck,DocumentCheck,LetterCheck;
    @FXML private Letter controlSupplyClass;
    @FXML private TextField RecipientSurname,RecipientName,SenderSurname,SenderName,Address,SIZETEXT,DELIVER,TRACK;
    @FXML private ComboBox Priority;
    private ObservableList<String> List = FXCollections.observableArrayList("Fast","Secret","Normal");
    @FXML
    public void  setsupply(ControllerClass SupplyControllerClass){this.SupplyControllerClass = SupplyControllerClass;}
    public  void  setList(ControllerClass control){
        this.control = control;
    }
    @FXML
    public void initialize(){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                Priority.setItems(List);
            }

        });
    }
    public boolean checkCustomerName(){
        for (Customer c : control.toListCustomer()){
            if (c.getName().equals(RecipientName.getText())){
                return true;
            }
        }
        return false;
    }
    public boolean checkCustomerSurname(){
        for (Customer c : control.toListCustomer()){
            if (c.getSurname().equals(RecipientSurname.getText())){
                    return true;
            }

        }
        return false;
    }
    public boolean checkCheckBox(){
        if (PackageCheck.isSelected() && DocumentCheck.isSelected() || PackageCheck.isSelected() && LetterCheck.isSelected() || DocumentCheck.isSelected() && LetterCheck.isSelected()){
            return false;
        }
        if (!PackageCheck.isSelected() && !DocumentCheck.isSelected() && !LetterCheck.isSelected()) return  false;
        else {
            if (PackageCheck.isSelected()){
                controlSupplyClass = new Package();
                controlSupplyClass.setName(RecipientName.getText());
                controlSupplyClass.setSurname(RecipientSurname.getText());
            }
            else if (DocumentCheck.isSelected()){
                controlSupplyClass = new Document();
                controlSupplyClass.setName(RecipientName.getText());
                controlSupplyClass.setSurname(RecipientSurname.getText());
            }
            else if (LetterCheck.isSelected()){
                controlSupplyClass = new Letter();
                controlSupplyClass.setName(RecipientName.getText());
                controlSupplyClass.setSurname(RecipientSurname.getText());
            }
        }
        return true;


    }
    @FXML public void NextOnHandle(ActionEvent event) throws IOException {
        if (!RecipientName.getText().isEmpty()){
            if (!RecipientSurname.getText().isEmpty()){
                if (checkCustomerName()) {
                    if (checkCustomerSurname()){
                        if (checkCheckBox()){
                            if (!SenderName.getText().isEmpty() && !SenderSurname.getText().isEmpty()){
                                controlSupplyClass.setSentName(SenderName.getText());
                                controlSupplyClass.setSentSurname(SenderSurname.getText());
                                if (!Address.getText().isEmpty()){
                                    controlSupplyClass.setAddress(Address.getText());
                                    if (!SIZETEXT.getText().isEmpty()){
                                        try {
                                            Double size = Double.parseDouble(SIZETEXT.getText());
                                            controlSupplyClass.setSize(size);
                                            if (controlSupplyClass.getClass().equals(Letter.class)){
                                                Button b = (Button) event.getSource();
                                                Stage stage = (Stage) b.getScene().getWindow();
                                                FXMLLoader loader = new FXMLLoader(getClass().getResource("/editsupplytwo.fxml"));
                                                stage.setScene(new Scene(loader.load(),800,600));
                                                EditSupplySubmit dw = loader.getController();
                                                dw.setList(control);
                                                dw.setControlSupplyClass(controlSupplyClass);
                                                dw.setsupply(SupplyControllerClass);
                                                stage.show();
                                            }
                                            else if (controlSupplyClass.getClass().equals(Document.class)){
                                                try {
                                                    ((Document)controlSupplyClass).setPriority(Priority.getValue().toString());
                                                    Button b = (Button) event.getSource();
                                                    Stage stage = (Stage) b.getScene().getWindow();
                                                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/editsupplytwo.fxml"));
                                                    stage.setScene(new Scene(loader.load(),800,600));
                                                    EditSupplySubmit dw = loader.getController();
                                                    dw.setList(control);
                                                    dw.setControlSupplyClass(controlSupplyClass);
                                                    dw.setsupply(SupplyControllerClass);
                                                    stage.show();
                                                }
                                                catch (NullPointerException e){
                                                    e = new NullPointerException("Please Select Priority");
                                                    label.setText(e.getMessage());
                                                }


                                            }
                                            else if (controlSupplyClass.getClass().equals(Package.class)){
                                                if (!TRACK.getText().isEmpty() && !DELIVER.getText().isEmpty()){
                                                    ((Package)controlSupplyClass).setTrack(TRACK.getText());
                                                    ((Package)controlSupplyClass).setDelivery(DELIVER.getText());
                                                    Button b = (Button) event.getSource();
                                                    Stage stage = (Stage) b.getScene().getWindow();
                                                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/editsupplytwo.fxml"));
                                                    stage.setScene(new Scene(loader.load(),800,600));
                                                    EditSupplySubmit dw = loader.getController();
                                                    dw.setList(control);
                                                    dw.setControlSupplyClass(controlSupplyClass);
                                                    dw.setsupply(SupplyControllerClass);
                                                    stage.show();
                                                }
                                                else label.setText("Please Input Track and Deliver");

                                            }


                                        }
                                        catch (IllegalArgumentException e){
                                            e =   new IllegalArgumentException("SIZE MUST BE NUMBER");
                                            label.setText(e.getMessage());
                                        }
                                    }
                                    else label.setText("PLEASE INPUT SIZE");

                                }
                                else label.setText("PLEASE INPUT ADDRESS");

                            }
                            else label.setText("Please Input SenderName and Surname");
                        }
                        else label.setText("Select only one BOX");
                    }
                    else label.setText("Your input is not match on any Customer Surname List");
                }
                else {
                    label.setText("Your input is not match on any Customer Name List");}
            }
            else label.setText("Please input Recipient Surname");
        }
        else label.setText("Please input Recipient name");


    }


    @FXML public void BACKOnHandle(ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/letterhandle.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));

        //check current
        LetterDisplay dw = loader.getController();
        dw.setList(control);



        stage.show();
    }

}
