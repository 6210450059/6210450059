package CondoProject.Services.EditSupply;

import CondoProject.Model.Condo;
import CondoProject.Data.DataB;
import CondoProject.Data.ReadWriteFile.DataSupplyFileSource;
import CondoProject.Model.supply.Document;
import CondoProject.Model.supply.Letter;
import CondoProject.Model.supply.Package;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import CondoProject.Model.People.Customer;
import CondoProject.Controller.ControllerClass;
import CondoProject.Services.DisplayTable.LetterDisplay;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class EditSupplySubmit {
    private ControllerClass control;
    @FXML private Button BBTN,SBBTN,PBTN;
    private ControllerClass SupplyControllerClass;
    @FXML private Label TRACK,SENDDERNAME,SENDDERSURNAME,ADDRESS,SIZE,priority,Delivery,label;
    @FXML private BorderPane borderpane;
    @FXML private ImageView image;
    private DataB dataCustomerFileSource;
    @FXML
    private Letter controlSupplyClass;
    public  void  setList(ControllerClass control){
        this.control = control;
    }
    public void  setsupply(ControllerClass SupplyControllerClass){this.SupplyControllerClass = SupplyControllerClass;}
    public void setControlSupplyClass(Letter controlSupplyClass){this.controlSupplyClass = controlSupplyClass;}
    @FXML
    public void initialize(){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                dataCustomerFileSource = new DataSupplyFileSource("data", "supply.csv");
                System.out.println(controlSupplyClass.toString());
                SENDDERNAME.setText("SENDER NAME "+controlSupplyClass.getSentName());
                SENDDERSURNAME.setText("SENDER SURNAME "+controlSupplyClass.getSentSurname());
                ADDRESS.setText("ADDRESS "+controlSupplyClass.getAddress());
                SIZE.setText(String.valueOf("SIZE "+controlSupplyClass.getSize()));
                if (controlSupplyClass.getClass().equals(Package.class)){
                    TRACK.setText("TRACK "+((Package)controlSupplyClass).getTrack());
                    Delivery.setText("DELIVERY "+((Package)controlSupplyClass).getDelivery());
                }
                else if (controlSupplyClass.getClass().equals(Document.class)){
                    priority.setText("PRIORITY "+((Document)controlSupplyClass).getPriority());
                }


            }
        });
    }
    @FXML public void BACKOnHandle(ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/editsupplyone.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));

        //check current
        EditSupplyRegister dw = loader.getController();
        dw.setList(control);
        dw.setsupply(SupplyControllerClass);



        stage.show();
    }
        @FXML public void SETPICTUREOnAction(ActionEvent event){
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select Image");
        Stage stage = (Stage) borderpane.getScene().getWindow();
        File file = fileChooser.showOpenDialog(stage);
        if (file != null){
            image.setImage(new Image(file.toURI().toString()));
            controlSupplyClass.setPicture(file.toURI().toString());
            System.out.println(controlSupplyClass.getPicture());
        }
    }
    public void setSupplyRoom(){
        for (Customer c: control.toListCustomer()){
            if (c.getName().equals(controlSupplyClass.getName()) && c.getSurname().equals(controlSupplyClass.getSurname())){
                controlSupplyClass.setCondo(new Condo(c.getBuilding(),c.getLayer(),c.getRoom(),c.getRoomType()));
                break;
            }
        }

    }
    @FXML public void SUBMITONACTION(ActionEvent event) throws IOException {
        if (controlSupplyClass.getPicture().isEmpty()){
            label.setText("Please Select Picture");
        }
        else{
            LocalDateTime date = LocalDateTime.now();
            DateTimeFormatter simpleDateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH:mm:ss");
            String strdate = simpleDateFormat.format(date);
            controlSupplyClass.setSimpleDateFormat(strdate);
            controlSupplyClass.setDay(strdate.substring(0,10));
            controlSupplyClass.setTime(strdate.substring(11,19));
            System.out.println(controlSupplyClass.toString());
            setSupplyRoom();
            SupplyControllerClass.addsupply(controlSupplyClass);
            dataCustomerFileSource.setData(SupplyControllerClass);
            Button b = (Button) event.getSource();
            Stage stage = (Stage) b.getScene().getWindow();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/letterhandle.fxml"));
            stage.setScene(new Scene(loader.load(),800,600));

            //check current
            LetterDisplay dw = loader.getController();
            dw.setsupply(SupplyControllerClass);
            dw.setList(control);


            stage.show();
        }
    }
}
