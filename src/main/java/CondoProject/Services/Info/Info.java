package CondoProject.Services.Info;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import CondoProject.Controller.ControllerClass;
import CondoProject.Services.Login;

import java.io.IOException;

public class Info {
    @FXML private Button BBTN;
    private ControllerClass control;
    public  void  setList(ControllerClass control){
     this.control =control;
    }
    @FXML
    public void initialize(){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {

            }
        });
    }
    @FXML public void HomeOnHandle(ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/login.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));

        //check current
        Login dw = loader.getController();
        stage.show();
        dw.setList(control);
    }
}
