package CondoProject.Services.SelectMenuOrBuilding;

import CondoProject.Services.DisplayTable.RoomDisplay;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import CondoProject.Model.People.Customer;
import CondoProject.Controller.ControllerClass;

import java.io.IOException;


public class SelectBuildingMenu {
    private ControllerClass control;
    @FXML private Button B1BTN,BBTN,B2BTN;
    @FXML private Label labelName,labelMenu;

    public  void  setList(ControllerClass control){
        this.control = control;
    }
    @FXML
    public void initialize(){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                labelName.setText(control.getClassType().toString());
                labelMenu.setText("Select Building");
            }
        });
    }
    @FXML public void Building1OnAction(ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        control.setCondoArrayList(control.toListCondoSeparate("1"));
        ObservableList<Customer> listObservableList = FXCollections.observableArrayList(control.toListShowRoom());
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/roomshow.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));
        //check current
        RoomDisplay dw = loader.getController();
        dw.setList(control);
        dw.setListObservableList(listObservableList);



        stage.show();
    }
    @FXML public void Building2OnAction(ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        control.setCondoArrayList(control.toListCondoSeparate("2"));
        ObservableList<Customer> listObservableList = FXCollections.observableArrayList(control.toListShowRoom());
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/roomshow.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));

        //check current
        RoomDisplay dw = loader.getController();
        dw.setList(control);
        dw.setListObservableList(listObservableList);


        stage.show();
    }
    @FXML public void BackOnAction(ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/informationcus.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));

        //check current
        InformationCustomerMenu dw = loader.getController();
        dw.setList(control);


        stage.show();
    }

}
