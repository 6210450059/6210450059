package CondoProject.Services.SelectMenuOrBuilding;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import CondoProject.Controller.ControllerClass;
import CondoProject.Services.DisplayTable.LetterDisplay;


import java.io.IOException;

public class EmployeeMenu {
    private ControllerClass control;
    public  void  setList(ControllerClass control){
        this.control = control;
    }
    @FXML private Button FILLBTN,SHOWBTN,HOME;
    @FXML private Label label;
    @FXML
    public void initialize(){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                label.setText(control.getClassType().toString());
            }
        });
    }
    @FXML public  void CustomerInformation(ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/informationcus.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));

        //check current
        InformationCustomerMenu dw = loader.getController();
        dw.setList(control);



        stage.show();
    }
    @FXML public void LetterOnHandle(ActionEvent event) throws  IOException{
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/letterhandle.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));

        //check current
        LetterDisplay dw = loader.getController();
        dw.setList(control);



        stage.show();
    }
    @FXML public void HomeOnHandle(ActionEvent event) throws  IOException{
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/login.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));




        stage.show();
    }

}
