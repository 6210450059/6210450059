package CondoProject.Services.SelectMenuOrBuilding;

import CondoProject.Services.Addroom.AddRoomSelectBuilding;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import CondoProject.Controller.ControllerClass;
import CondoProject.Services.Register.Register;

import java.io.IOException;

public class InformationCustomerMenu {
    private ControllerClass control;
    public  void  setList(ControllerClass control){
        this.control = control;
    }
    @FXML private Button RBTN,CRBTN,ERBTN,BBTN;
    @FXML private Label labelName,labelMenu;
    @FXML
    public void initialize(){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                labelMenu.setText("Menu");
                labelName.setText(control.getClassType().toString());

            }
        });
    }

    @FXML public void BACKOnHandle(ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/employeehomepage.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));

        //check current
        EmployeeMenu dw = loader.getController();
        dw.setList(control);



        stage.show();
    }
    @FXML public void RegisterCustomerOnHandle(ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/register.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));

        //check current
        Register dw = loader.getController();
        dw.setList(control);



        stage.show();
    }
    @FXML public void RoomShowOnAction(ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/selectbuilding.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));

        //check current
        SelectBuildingMenu dw = loader.getController();
        dw.setList(control);



        stage.show();
    }
    @FXML public void EditRoomOnAction(ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/addroom.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));

        //check current
        AddRoomSelectBuilding dw = loader.getController();
        dw.setList(control);



        stage.show();
    }
}
