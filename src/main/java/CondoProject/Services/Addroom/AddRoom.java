package CondoProject.Services.Addroom;

import CondoProject.Data.DataB;
import CondoProject.Data.ReadWriteFile.DataCondoFileSource;
import javafx.application.Platform;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;


import javafx.stage.Stage;
import CondoProject.Model.Condo;




import CondoProject.Controller.ControllerClass;
import CondoProject.Controller.StringConfiguration;


import java.io.IOException;
import java.util.ArrayList;

public class AddRoom {
    private ControllerClass control;
    @FXML private Button BBTN,SBTN;
    @FXML private TableView<Condo> table;
    private String string;

    public void setString(String string) {
        this.string = string;
    }


    @FXML private Label LabelMenu,ERR;
    @FXML private ComboBox<String> TYPEROOM,ROOMLAYER,ROOMNUM;
    private ObservableList<Condo> listObservableList ;
    public  void  setList(ControllerClass control){
        this.control = control;
    }
    @FXML
    public void initialize(){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                LabelMenu.setText("ADD ROOM");



                TYPEROOM.setItems(FXCollections.observableArrayList("DeluxeRoom","StandardRoom"));
                ROOMNUM.setItems(FXCollections.observableArrayList("01","02","03","04","05","06","07","08","09","10"));
                ROOMLAYER.setItems(FXCollections.observableArrayList("1","2","3","4","5","6","7","8","9"));
                ShowTable();
            }
        });




    }
    public void ShowTable(){
        listObservableList = FXCollections.observableArrayList(control.toListCondoSeparate(string));
        table.setItems(listObservableList);
        ArrayList<StringConfiguration> configs = new ArrayList<>();
        configs.add(new StringConfiguration("title:Layer","field:layers"));
        configs.add(new StringConfiguration("title:ROOM","field:room"));
        configs.add(new StringConfiguration("title:RoomTYPE", "field:roomType"));


        for (StringConfiguration conf : configs){
            TableColumn col = new TableColumn(conf.get("title"));
            col.setCellValueFactory(new PropertyValueFactory<>(conf.get("field")));
            table.getColumns().add(col);
            if (conf.get("title").equals("ROOM")){
                table.getSortOrder().addAll(col);
                col.setSortType(TableColumn.SortType.ASCENDING);
            }
        }


    }
    public boolean CheckRoom(String roomlayer, String roomnum, String typeroom){
        for (Condo c : control.toListCondoSeparate(string)){
            if (c.getLayers().equals(roomlayer) && c.getRoom().equals(roomlayer+roomnum)){
                return true;
            }
        }
        return false;
    }
    @FXML public void BackOnAction(ActionEvent event) throws IOException {

        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/addroom.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));

        //check current
        AddRoomSelectBuilding dw = loader.getController();
        dw.setList(control);
        dw.setString(string);

        stage.show();
    }

    @FXML public void SubmitOnAction(ActionEvent event)throws IOException {
        DataB dataFileSource = new DataCondoFileSource("data", "condo.csv");
        if (!ROOMLAYER.getSelectionModel().isEmpty()){
            if (!ROOMNUM.getSelectionModel().isEmpty()){
                if (!TYPEROOM.getSelectionModel().isEmpty()){
                    if (CheckRoom(ROOMLAYER.getValue(),ROOMNUM.getValue(),TYPEROOM.getValue())){
                        ERR.setText("Your Room is already Have");
                    }
                    else{
                        Condo addCondo = new Condo(string,ROOMLAYER.getValue(),ROOMLAYER.getValue()+ROOMNUM.getValue(),TYPEROOM.getValue());
                        control.addRoom(addCondo);

                        table.refresh();
                        listObservableList.clear();
                        listObservableList = FXCollections.observableArrayList(control.toListCondoSeparate(string));
                        table.setItems(listObservableList);
                        dataFileSource.setData(control);


                    }
                }
                else ERR.setText("Please select Room Type");
            }
            else
            ERR.setText("Please select Room Number");

        }
        else
        ERR.setText("Please select Room Layer");



    }
}
