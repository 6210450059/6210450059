package CondoProject.Services.Addroom;

import CondoProject.Services.SelectMenuOrBuilding.InformationCustomerMenu;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import CondoProject.Controller.ControllerClass;


import java.io.IOException;

public class AddRoomSelectBuilding {
    private ControllerClass control;
    private String string;
    public void setString(String string) {
        this.string = string;
    }
    @FXML private Button B1BTN,BBTN,B2BTN;
    @FXML private Label labelName,labelMenu;
    public  void  setList(ControllerClass control){
        this.control = control;
    }
    @FXML
    public void initialize(){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                labelName.setText(control.getClassType().toString());
                labelMenu.setText("Select Building");
            }
        });
    }

    @FXML public void BackOnAction(ActionEvent event) throws IOException {

        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/informationcus.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));

        //check current
        InformationCustomerMenu dw = loader.getController();
        dw.setList(control);


        stage.show();
    }
    @FXML public void Building1OnAction(ActionEvent event) throws IOException {
        string = "1";
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/addroomnext.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));

        AddRoom dw = loader.getController();
        dw.setList(control);
        dw.setString(string);






        stage.show();
    }
    @FXML public void Building2OnAction(ActionEvent event) throws IOException {
        string = "2";
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/addroomnext.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));
        //check current
        AddRoom dw = loader.getController();
        dw.setList(control);
        dw.setString(string);





        stage.show();
    }
}
