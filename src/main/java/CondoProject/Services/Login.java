package CondoProject.Services;
import CondoProject.Model.People.Employee;
import CondoProject.Data.ReadWriteFile.DataCondoFileSource;
import CondoProject.Data.ReadWriteFile.DataEmployeeFileSource;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import CondoProject.Services.DisplayTable.AdminDisplayEmployee;
import CondoProject.Controller.ControllerClass;
import CondoProject.Data.DataB;
import CondoProject.Data.ReadWriteFile.DataCustomerFileSource;
import CondoProject.Services.SelectMenuOrBuilding.EmployeeMenu;
import CondoProject.Services.Info.Info;
import CondoProject.Services.Info.Profile;

import java.io.IOException;

public class Login {
    @FXML private TextField userField, passField;
    @FXML private Button loginBtn,PassBtn1,proBtn;
    @FXML private  Label label;
    private ControllerClass control;
    public  void  setList(ControllerClass control){
        this.control = control;
    }

    private DataB dataSource;
    private DataB dataSourceCondo;


    @FXML public void initialize(){ //constructor for fxml call when load screen
        control = new ControllerClass();
        dataSource = new DataCustomerFileSource("data", "customer.csv"); /*Read File*/
        control.setAccountList(dataSource.Read().toListCustomer());
        dataSource = new DataEmployeeFileSource("data","employee.csv");
        control.addAccountList(dataSource.Read().toListEmployee());
        dataSourceCondo = new DataCondoFileSource("data","condo.csv");

    }


    @FXML public  void SuccessLogin(ActionEvent event) throws IOException {
        /*check pin and id*/
        System.out.println(userField.getText() +" "+ passField.getText());


        if (control.Check(userField.getText(),passField.getText())){
                if ( control.getClassType().getClass().equals(Employee.class)){
                System.out.println(control.getClassType());

                Button b = (Button) event.getSource();
                Stage stage = (Stage) b.getScene().getWindow();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/employeehomepage.fxml"));
                stage.setScene(new Scene(loader.load(),800,600));
                control.setCondoArrayList(dataSourceCondo.Read().toListCondo());
                dataSource = new DataEmployeeFileSource("data","employee.csv");
                control.setAccountList(dataSource.Read().toListEmployee());
                control.settime();
                dataSource.setData(control);
                control.setAccountList(dataSource.Read().toListEmployee());
                dataSource.setData(control);
                dataSource = new DataCustomerFileSource("data","customer.csv");
                control.addAccountList(dataSource.Read().toListCustomer());
                EmployeeMenu dw = loader.getController();
                dw.setList(control);
                stage.show();}
        }
        else if (userField.getText().equals("admin")&&passField.getText().equals("admin")){
            Button b = (Button) event.getSource();
            Stage stage = (Stage) b.getScene().getWindow();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/adminhomepage.fxml"));
            stage.setScene(new Scene(loader.load(),800,600));

            //check current
            AdminDisplayEmployee dw = loader.getController();
            dw.setList(control);
            stage.show();
        }
        else {
            label.setText("Username or password not correct");
        }
    }
    @FXML public void InfoOnAction(ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/infopage1.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));

        //check current
        Info dw = loader.getController();
        stage.show();
        dw.setList(control);
    }
    @FXML public void ProfileOnAction(ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/profile.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));

        //check current
        Profile dw = loader.getController();
        stage.show();
        dw.setList(control);

    }
}
