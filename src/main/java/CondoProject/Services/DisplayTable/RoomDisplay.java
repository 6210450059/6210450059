package CondoProject.Services.DisplayTable;
/*CustomerHomepage*/
import CondoProject.Data.DataB;
import CondoProject.Data.ReadWriteFile.DataCondoFileSource;
import CondoProject.Services.SelectMenuOrBuilding.SelectBuildingMenu;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import CondoProject.Model.People.Customer;
import CondoProject.Controller.ControllerClass;
import CondoProject.Controller.StringConfiguration;

import java.io.IOException;
import java.util.ArrayList;

public class RoomDisplay {
    @FXML private Label currentLabel;
    @FXML private Button BBTN;
    private ControllerClass control;
    public void  setListObservableList(ObservableList<Customer> listObservableList){
        this.listObservableList = listObservableList;
    }
    public  void  setList(ControllerClass control){
        this.control = control;
    }
    @FXML private TableView<Customer> table;
    public ObservableList<Customer> listObservableList;
    @FXML
    public void initialize(){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                currentLabel.setText(control.getClassType().toString());
                ShowTable();
            }
        });
    }
    public void ShowTable(){
        table.setItems(listObservableList);
        ArrayList<StringConfiguration> configs = new ArrayList<>();
        configs.add(new StringConfiguration("title:Layer","field:layer"));
        configs.add(new StringConfiguration("title:Room","field:room"));
        configs.add(new StringConfiguration("title:RoomType","field:roomType"));
        configs.add(new StringConfiguration("title:Name", "field:name"));
        configs.add(new StringConfiguration("title:Surname", "field:surname"));

        for (StringConfiguration conf: configs) {
            TableColumn col = new TableColumn(conf.get("title"));
            col.setCellValueFactory(new PropertyValueFactory<>(conf.get("field")));
            table.getColumns().add(col);
            if (conf.get("title").equals("Room")){
                table.getSortOrder().addAll(col);
                col.setSortType(TableColumn.SortType.ASCENDING);
            }
        }

    }
    @FXML public void BackOnAction(ActionEvent event) throws IOException {
        DataB dataSourceCondo = new DataCondoFileSource("data","condo.csv");
        control.setCondoArrayList(dataSourceCondo.Read().toListCondo());
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/selectbuilding.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));

        //check current
        SelectBuildingMenu dw = loader.getController();
        dw.setList(control);


        stage.show();
    }
}
