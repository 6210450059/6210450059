package CondoProject.Services.DisplayTable;

import CondoProject.Model.People.Customer;
import CondoProject.Services.Register.RegisterEmployee.RegisterEmployee;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import CondoProject.Controller.ControllerClass;
import CondoProject.Controller.StringConfiguration;
import CondoProject.Data.ReadWriteFile.DataCustomerFileSource;
import CondoProject.Services.Login;

import java.io.IOException;
import java.util.ArrayList;

public class AdminDisplayEmployee {
    private ControllerClass control;                    /*ControllerClass control deal with Customer Employee and Condo*/

    public  void  setList(ControllerClass control){
        this.control = control;
    }
    public ObservableList<Customer> listObservableList;
    @FXML private Button HBTN,RBTN;
    @FXML private TableView<Customer> table;
    private DataCustomerFileSource dataSource;
    @FXML private Label labelName;
    @FXML
    public void initialize(){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                labelName.setText("Welcome Admin");
                listObservableList = FXCollections.observableArrayList(control.toListEmployee());
                ShowTable();
            }
        });
    }
    public void ShowTable(){
        table.setItems(listObservableList);
        ArrayList<StringConfiguration> configs = new ArrayList<>();
        configs.add(new StringConfiguration("title:Day","field:day"));
        configs.add(new StringConfiguration("title:TIME","field:time"));
        configs.add(new StringConfiguration("title:Name", "field:name"));
        configs.add(new StringConfiguration("title:ID", "field:id"));
        configs.add(new StringConfiguration("title:Password", "field:pin"));




        for (StringConfiguration conf : configs){
            TableColumn col = new TableColumn(conf.get("title"));
            col.setCellValueFactory(new PropertyValueFactory<>(conf.get("field")));
            table.getColumns().add(col);
            if (conf.get("title").equals("Day")){
                table.getSortOrder().addAll(col);
                col.setSortType(TableColumn.SortType.DESCENDING);
            }
            if (conf.get("title").equals("TIME")){
                table.getSortOrder().addAll(col);
                col.setSortType(TableColumn.SortType.DESCENDING);
            }
        }
    }
    @FXML public void HomeOnHandle(ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/login.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));

        //check current
        Login dw = loader.getController();
        dw.setList(control);



        stage.show();
    }
    @FXML public void RegisterOnAction(ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/registeremployee.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));

        //check current
        RegisterEmployee dw = loader.getController();
        dw.setList(control);



        stage.show();
    }
}
