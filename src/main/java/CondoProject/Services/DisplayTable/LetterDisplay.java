package CondoProject.Services.DisplayTable;

import CondoProject.Data.DataB;
import CondoProject.Data.ReadWriteFile.DataSupplyFileSource;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import CondoProject.Model.supply.Document;
import CondoProject.Model.supply.Letter;
import CondoProject.Model.supply.Package;
import CondoProject.Controller.ControllerClass;
import CondoProject.Controller.StringConfiguration;
import CondoProject.Services.SelectMenuOrBuilding.EmployeeMenu;
import CondoProject.Services.EditSupply.EditSupplyRegister;


import java.io.IOException;
import java.util.ArrayList;

public class LetterDisplay {
    private ControllerClass control;
    public  void  setList(ControllerClass control){
        this.control = control;
    }
    private ControllerClass SupplyControllerClass;
    @FXML private TableView<Letter> table;
    private ObservableList<Letter> listObservableList;
    @FXML private
    Label labelname,labelsurname,labelsentName,labelsentsurname,labelnametype,labeladdress;
    @FXML private  Button LBTN,BBTN,EBTN,DELBTN;
    @FXML private ImageView image;

    public void  setsupply(ControllerClass SupplyControllerClass ){this.SupplyControllerClass = SupplyControllerClass;}
    @FXML
    public void initialize(){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                DataB dataSource = new DataSupplyFileSource("data", "supply.csv");
                SupplyControllerClass = dataSource.Read();
                for (Letter l : control.toListSupply()){
                    System.out.println(l.toString());
                }
                listObservableList = FXCollections.observableArrayList(SupplyControllerClass.toListSupply());
                ShowTable();
                showlabel();
            }
        });
    }
    public void ShowTable(){
        table.setItems(listObservableList);
        ArrayList<StringConfiguration> configs = new ArrayList<>();
        configs.add(new StringConfiguration("title:Checkbox", "field:checkBox"));
        configs.add(new StringConfiguration("title:DAY","field:day"));
        configs.add(new StringConfiguration("title:TIME","field:time"));
        configs.add(new StringConfiguration("title:Name", "field:name"));
        configs.add(new StringConfiguration("title:Surname", "field:surname"));
        configs.add(new StringConfiguration("title:SentName", "field:sentName"));
        configs.add(new StringConfiguration("title:SentSurname", "field:sentSurname"));
        configs.add(new StringConfiguration("title:Address", "field:address"));
        configs.add(new StringConfiguration("title:Size", "field:size"));
        configs.add(new StringConfiguration("title:Priority", "field:priority"));
        configs.add(new StringConfiguration("title:Delivery", "field:delivery"));
        configs.add(new StringConfiguration("title:Track", "field:track"));


        for (StringConfiguration conf: configs) {
            TableColumn col = new TableColumn(conf.get("title"));
            col.setCellValueFactory(new PropertyValueFactory<>(conf.get("field")));
            table.getColumns().add(col);
            if (conf.get("title").equals("DAY")){
                table.getSortOrder().addAll(col);
                col.setSortType(TableColumn.SortType.DESCENDING);
            }
            if (conf.get("title").equals("TIME")){
                table.getSortOrder().addAll(col);
                col.setSortType(TableColumn.SortType.DESCENDING);
            }
        }
    }
    @FXML public void BACKOnHandle(ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/employeehomepage.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));

        //check current
        EmployeeMenu dw = loader.getController();
        dw.setList(control);




        stage.show();
    }
    @FXML public void EditSupplyOnHandle(ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/editsupplyone.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));

        //check current
        EditSupplyRegister dw = loader.getController();
        dw.setList(control);
        dw.setsupply(SupplyControllerClass);



        stage.show();
    }
    @FXML public void DeleteSelectOnAction(ActionEvent event) {
//        table.getItems().removeAll(table.getSelectionModel().getSelectedItem());
        ObservableList<Letter> dataListRemove = FXCollections.observableArrayList();
        for (Letter con :listObservableList){
            if (con.getCheckBox().isSelected()){
                dataListRemove.add(con);
            }
        }
        listObservableList.removeAll(dataListRemove);
        SupplyControllerClass = new ControllerClass();
        for (Letter con: listObservableList){
            SupplyControllerClass.addsupply(con);
        }
        DataB dataCustomerFileSource = new DataSupplyFileSource("data", "supply.csv");
        dataCustomerFileSource.setData(SupplyControllerClass);
    }
    public void showlabel(){
        table.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    Letter c1 = table.getItems().get(table.getSelectionModel().getSelectedIndex());
                    labelname.setText("NAME: "+c1.getName());
                    labelsurname.setText("SURNAME: "+c1.getSurname());
                    labelsentName.setText("SENDER NAME: "+c1.getSentName());
                    labelsentsurname.setText("SENDER SURNAME: "+c1.getSentSurname());
                    labeladdress.setText("Address: "+ c1.getAddress());
                    if (c1.getClass().equals(Letter.class)){
                        labelnametype.setText("TYPE: Letter");
                    }
                    else if (c1.getClass().equals(Document.class)){
                        labelnametype.setText("TYPE: Document");
                    }
                    else if (c1.getClass().equals(Package.class)){
                        labelnametype.setText("TYPE: Package");
                    };
                    if (c1.getPicture() != null){
                        image.setImage(new Image(c1.getPicture()));
                    }

                }
                catch (ArrayIndexOutOfBoundsException e){

                }



            }
        });
    }
}
