package CondoProject.Services.Register.RegisterCustomer;

import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import CondoProject.Model.Condo;
import CondoProject.Model.People.Customer;
import CondoProject.Controller.ControllerClass;

import java.io.IOException;

public class RegisterCustomer {
    private ControllerClass control;
    private Customer customerRegister;
    @FXML private TextField Name,Surname;
    @FXML private Label label;
    @FXML private Label labelName,labelMenu;
    @FXML private ComboBox<Condo> room;
    private ObservableList<Condo> condoList;
    @FXML Button SubBtn,BBTN;

    public  void  setList(ControllerClass control){
        this.control = control;
    }
    public void  setRegister(Customer customerRegister){this.customerRegister=customerRegister; }
    public void setCondoList(ObservableList<Condo> condoList){
        this.condoList = condoList;
    }
    public void initialize(){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                labelName.setText(control.getClassType().toString());
                labelMenu.setText("Input Name and Surname");
                System.out.println(customerRegister.getId()+" "+customerRegister.getPin());
                room.setItems(condoList);
            }
        });
    }
    public Boolean CheckDeluxe(){
        int count = 0;
        for (Customer acc: control.toListCustomer()){
            if (acc.getBuilding().equals(customerRegister.getBuilding())&&acc.getLayer().equals(customerRegister.getLayer())&&acc.getRoom().equals(customerRegister.getRoom())){
                if (count == 1) return false;
                count +=1;
            }
        }
        return true;
    }
    public Boolean CheckStandard(){
        for (Customer acc: control.toListCustomer()){
            if (acc.getBuilding().equals(customerRegister.getBuilding())&&acc.getLayer().equals(customerRegister.getLayer())&&acc.getRoom().equals(customerRegister.getRoom())){
                 return false;
            }
        }
        return true;
    }
    @FXML public  void SubmitOnAction (ActionEvent event) throws IOException {
        /*check pin and id*/
        if (!Name.getText().isEmpty()) {
            if (!Surname.getText().isEmpty()) {
                try {
                    boolean boo;
                    label.setText(room.getValue().toString());
                    customerRegister.setName(Name.getText());
                    customerRegister.setSurname(Surname.getText());
                    customerRegister.setRoom(room.getValue().getRoom());
                    customerRegister.setBuilding(room.getValue().getBuilding());
                    customerRegister.setLayer(room.getValue().getLayers());
                    customerRegister.setRoomType(room.getValue().getRoomType());
                    System.out.println(customerRegister.toString());
                    if (customerRegister.getRoomType().equals("StandardRoom")){
                        boo = CheckStandard();
                    }
                    else{
                        boo = CheckDeluxe();
                    }
                    if (boo) {
                        Button b = (Button) event.getSource();
                        Stage stage = (Stage) b.getScene().getWindow();
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("/registercustomersubmit.fxml"));
                        stage.setScene(new Scene(loader.load(), 800, 600));

                        //check current
                        RegisterCustomerSubmit dw = loader.getController();
                        dw.setList(control);
                        dw.setRegister(customerRegister);
                        dw.setCondoList(condoList);


                        stage.show();
                    } else {
                        label.setText("Room is Full");

                    }
                }
                catch (NullPointerException e){
                    e = new NullPointerException("Please Select Room");
                    label.setText(e.getMessage());
                                            }

            }
            else label.setText("Please input your Surname");
        }
        else label.setText("Please input your Name");



    }
    @FXML public  void BackOnAction(ActionEvent event) throws IOException {

        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/registercustomer.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));

        //check current
        RegisterCustomerSelectBuilding dw = loader.getController();
        dw.setList(control);
        dw.setRegister(customerRegister);



        stage.show();


    }
}
