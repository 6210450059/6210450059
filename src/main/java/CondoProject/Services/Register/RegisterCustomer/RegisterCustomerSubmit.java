package CondoProject.Services.Register.RegisterCustomer;

import CondoProject.Model.Condo;
import CondoProject.Data.DataB;
import CondoProject.Services.SelectMenuOrBuilding.InformationCustomerMenu;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import CondoProject.Model.People.Customer;
import CondoProject.Controller.ControllerClass;
import CondoProject.Data.ReadWriteFile.DataCustomerFileSource;

import java.io.IOException;

public class RegisterCustomerSubmit {
    private ControllerClass control;
    private Customer customerRegister;
    private ObservableList<Condo> condoList;
    @FXML private Button SUBBTN,BBTN;
    @FXML private Label labelMenu;

    @FXML private Label NAME,SURNAME,USERNAME,PASSWORD,BUILDING;
    public void setCondoList(ObservableList<Condo> condoList){
        this.condoList = condoList;
    }

    public  void  setList(ControllerClass control){
        this.control = control;
    }
    public void  setRegister(Customer customerRegister){this.customerRegister=customerRegister; }

    public void initialize(){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                labelMenu.setText("Submit");
                NAME.setText("NAME: "+customerRegister.getName());
                SURNAME.setText("SURNAME: "+customerRegister.getSurname());
                USERNAME.setText("USERID: "+customerRegister.getId());
                PASSWORD.setText("PASSWORD: "+customerRegister.getPin());
                BUILDING.setText("BUILDING: "+customerRegister.getBuilding()+", Layer: "+customerRegister.getLayer()+", Room: "+customerRegister.getRoom()+", RoomType: "+customerRegister.getRoomType());

            }
        });
    }
    @FXML public  void SubmitOnAction(ActionEvent event) throws IOException {
        DataB dataCustomerFileSource = new DataCustomerFileSource("data", "customer.csv");
        ControllerClass submitCustomer = new ControllerClass();
        submitCustomer.setAccountList(dataCustomerFileSource.Read().toListCustomer());
        submitCustomer.addDataList(customerRegister);
        control.addDataList(customerRegister);
        dataCustomerFileSource.setData(submitCustomer);

        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/informationcus.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));

        //check current
        InformationCustomerMenu dw = loader.getController();
        dw.setList(control);



        stage.show();


    }
    @FXML public  void BackOnAction(ActionEvent event) throws IOException {

        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/registercustomernext.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));

        //check current
        RegisterCustomer dw = loader.getController();
        dw.setList(control);
        dw.setRegister(customerRegister);
        dw.setCondoList(condoList);



        stage.show();


    }
}
