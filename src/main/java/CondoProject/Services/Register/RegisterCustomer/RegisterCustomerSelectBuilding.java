package CondoProject.Services.Register.RegisterCustomer;

import CondoProject.Services.Register.Register;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import CondoProject.Model.Condo;
import CondoProject.Model.People.Customer;
import CondoProject.Controller.ControllerClass;

import java.io.IOException;
import java.util.ArrayList;

public class RegisterCustomerSelectBuilding {
    private ControllerClass control;
    @FXML private Button B1,B2,BBTN;
    private Customer customerRegister;
    @FXML private Label labelName,labelMenu;
    public  void  setList(ControllerClass control){
        this.control = control;
    }
    public void  setRegister(Customer customerRegister){this.customerRegister=customerRegister; }
    public void initialize(){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                labelName.setText(control.getClassType().toString());
                labelMenu.setText("Select Building");
            }
        });
    }

    @FXML public  void Building1Access(ActionEvent event) throws IOException {

        ArrayList<Condo> condoArrayList = control.toListCondoSeparate("1");
        ObservableList<Condo> condoList = FXCollections.observableArrayList(condoArrayList);

        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/registercustomernext.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));

        //check current
        RegisterCustomer dw = loader.getController();
        dw.setList(control);
        dw.setRegister(customerRegister);
        dw.setCondoList(condoList);



        stage.show();


    }
    @FXML public  void Building2Access(ActionEvent event) throws IOException {
        ArrayList<Condo> condoArrayList = control.toListCondoSeparate("2");
        ObservableList<Condo> condoList = FXCollections.observableArrayList(condoArrayList);

        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/registercustomernext.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));

        //check current
        RegisterCustomer dw = loader.getController();
        dw.setList(control);
        dw.setRegister(customerRegister);
        dw.setCondoList(condoList);



        stage.show();


    }
    @FXML public  void BackOnAction(ActionEvent event) throws IOException {

        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/register.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));

        //check current
        Register dw = loader.getController();
        dw.setList(control);
        dw.setRegister(customerRegister);



        stage.show();


    }
}
