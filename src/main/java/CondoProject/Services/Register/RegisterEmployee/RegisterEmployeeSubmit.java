package CondoProject.Services.Register.RegisterEmployee;

import CondoProject.Data.DataB;
import CondoProject.Data.ReadWriteFile.DataEmployeeFileSource;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

import CondoProject.Services.DisplayTable.AdminDisplayEmployee;
import CondoProject.Model.People.Employee;
import CondoProject.Controller.ControllerClass;

import java.io.IOException;

public class RegisterEmployeeSubmit {
    @FXML private Button SBTN,BTN;
    @FXML private Label PASSWORD,USERNAME,SURNAME,NAME;
    private Employee employee;
    private ControllerClass control;
    private ControllerClass submitEmployee;
    private DataB dataCustomerFileSource;
    public void  setEmployee(Employee employee){this.employee = employee;}
    public  void  setList(ControllerClass control){
        this.control = control;
    }
    public void initialize(){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                NAME.setText("NAME: "+employee.getName());
                SURNAME.setText("SURNAME: "+employee.getSurname());
                USERNAME.setText("ID: "+employee.getId());
                PASSWORD.setText("PASSWORD: "+employee.getPin());
                dataCustomerFileSource = new DataEmployeeFileSource("data", "employee.csv");

            }
        });
    }
    @FXML public void BACKOnHandle(ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/registeremployee.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));

        //check current
        RegisterEmployee dw = loader.getController();
        dw.setList(control);




        stage.show();
    }
    @FXML public void SubmitOnHandle(ActionEvent event) throws IOException {
        submitEmployee = new ControllerClass();
        submitEmployee.setAccountList(dataCustomerFileSource.Read().toListEmployee());
        submitEmployee.addDataList(employee);
        control.addDataList(employee);
        dataCustomerFileSource.setData(submitEmployee);
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/adminhomepage.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));

        //check current
        AdminDisplayEmployee dw = loader.getController();
        dw.setList(control);




        stage.show();
    }
}
