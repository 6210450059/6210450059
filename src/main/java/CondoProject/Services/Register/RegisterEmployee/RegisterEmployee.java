package CondoProject.Services.Register.RegisterEmployee;

import CondoProject.Services.DisplayTable.AdminDisplayEmployee;
import CondoProject.Model.People.Customer;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import CondoProject.Model.People.Employee;
import CondoProject.Controller.ControllerClass;

import java.io.IOException;

public class RegisterEmployee {
    @FXML private
    PasswordField PASSWORD,REPASS;
    @FXML private
    TextField NAME,SURNAME,IDEN;
    @FXML private Button BBTN,NBTN;
    @FXML private Label label;
    private ControllerClass control;
    private Employee employee;
    public  void  setList(ControllerClass control){
        this.control = control;
    }
    public void initialize(){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {


            }
        });
    }
    @FXML public void BACKOnHandle(ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/adminhomepage.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));

        //check current
        AdminDisplayEmployee dw = loader.getController();
        dw.setList(control);




        stage.show();
    }
    public boolean CheckId(String text){
        for (Customer acc : control.toListEmployee()){
            if (acc.getId().equals(text))return false;
        }
        return true;
    }
    @FXML public void NextOnAction(ActionEvent event) throws IOException {
        if (!NAME.getText().isEmpty()){
            if (!SURNAME.getText().isEmpty()){
                if (!IDEN.getText().isEmpty()){
                        if (!PASSWORD.getText().isEmpty()){
                            if (!REPASS.getText().isEmpty()){
                                if (CheckId(IDEN.getText())){
                                    if (PASSWORD.getText().equals(REPASS.getText())){
                                            employee = new Employee();
                                            employee.setName(NAME.getText());
                                            employee.setSurname(SURNAME.getText());
                                            employee.setPin(PASSWORD.getText());
                                            employee.setId(IDEN.getText());
                                            employee.setTime("-");
                                            employee.setDay("-");
                                            System.out.println(employee.toString());
                                            Button b = (Button) event.getSource();
                                            Stage stage = (Stage) b.getScene().getWindow();
                                            FXMLLoader loader = new FXMLLoader(getClass().getResource("/registeremployeesubmit.fxml"));
                                            stage.setScene(new Scene(loader.load(),800,600));

                                            //check current
                                            RegisterEmployeeSubmit dw = loader.getController();
                                            dw.setList(control);
                                            dw.setEmployee(employee);
                                            stage.show();
                                    }
                                    else label.setText("Password is not the same");
                                }
                                else label.setText("This ID has been use");
                            }
                            else label.setText("Please Input Employee Repassword");
                        }
                        else label.setText("Please Input Employee Password");
                }
                else label.setText("Please Input Employee Identity ");
            }
            else label.setText("Please Input Employee Surname");
        }
        else label.setText("Please Input Employee Name");

    }

}
