package CondoProject.Services.Register;

import CondoProject.Services.Register.RegisterCustomer.RegisterCustomerSelectBuilding;
import javafx.application.Platform;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import CondoProject.Model.People.Customer;
import CondoProject.Controller.ControllerClass;
import CondoProject.Services.SelectMenuOrBuilding.InformationCustomerMenu;

import java.io.IOException;


public class Register {
    private ControllerClass control;
    private Customer customerRegister;
    public  void  setList(ControllerClass control){
        this.control = control;
    }
    public void  setRegister(Customer customerRegister){this.customerRegister=customerRegister; }
    @FXML private Button BBTN,NBTN;
    @FXML private TextField USER;
    @FXML private PasswordField REPASS,PASS;
    @FXML private Label labelName,labelMenu;
    @FXML private Label ERR;
    @FXML
    public void initialize(){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                labelName.setText(control.getClassType().toString());
                labelMenu.setText("Register Homepage");
            }
        });
    }
    @FXML public  void HOME (ActionEvent event) throws IOException {
        /*check pin and id*/

        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/informationcus.fxml"));
        stage.setScene(new Scene(loader.load(),800,600));

        //check current
        InformationCustomerMenu dw = loader.getController();
        dw.setList(control);



        stage.show();


    }
    public boolean CheckId(String text){
        for (Customer acc : control.toList()){
            if (acc.getId().equals(text))return true;
        }
        return false;
    }

    @FXML public void  createAccount(ActionEvent event) throws IOException{

        if (!USER.getText().isEmpty()&&!PASS.getText().isEmpty()&&!REPASS.getText().isEmpty()){
            if (CheckId(USER.getText())){
                ERR.setText("USERNAME already exits");
            }
            else if (!PASS.getText().equals(REPASS.getText())){
                ERR.setText("PASSWORD IS NOT SAME");
            }
            else if (PASS.getText().equals(REPASS.getText()) && PASS.getText().equals(USER.getText())){
                ERR.setText("your username should not be the same with password");
            }
            else{
                Button b = (Button) event.getSource();
                Stage stage = (Stage) b.getScene().getWindow();
                customerRegister = new Customer("","",USER.getText(),PASS.getText());
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/registercustomer.fxml"));
                stage.setScene(new Scene(loader.load(),800,600));

                //check current
                RegisterCustomerSelectBuilding dw = loader.getController();
                dw.setList(control);
                dw.setRegister(customerRegister);




                stage.show();
            }

        }
        else if (USER.getText().isEmpty()) ERR.setText("INPUT USERNAME");
        else if (PASS.getText().isEmpty()) ERR.setText("INPUT PASSWORD");
        else if (REPASS.getText().isEmpty()) ERR.setText("INPUT REPASSWORD");


    }

}
