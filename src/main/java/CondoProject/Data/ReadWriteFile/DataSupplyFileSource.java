package CondoProject.Data.ReadWriteFile;

import CondoProject.Controller.ControllerClass;
import CondoProject.Data.DataB;
import CondoProject.Model.supply.Document;
import CondoProject.Model.supply.Letter;
import CondoProject.Model.supply.Package;

import java.io.*;

public class DataSupplyFileSource implements DataB {
    private String fileDirectoryName;
    private String fileName;
    private ControllerClass supplies;

    public DataSupplyFileSource(String fileDirectoryName, String fileName) {
        this.fileDirectoryName = fileDirectoryName;
        this.fileName = fileName;
        checkFileIsExisted();
        supplies = new ControllerClass();
    }
    private void checkFileIsExisted() {
        File file = new File(fileDirectoryName);
        if (!file.exists()) {
            file.mkdirs();
        }
        String filePath = fileDirectoryName + File.separator + fileName;
        file = new File(filePath);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                System.err.println("Cannot create " + filePath);
            }
        }
    }
    private void ReadSupply() throws IOException{
        String filePath = fileDirectoryName + File.separator + fileName;
        File file = new File(filePath);
        FileReader fileReader = new FileReader(file);
        BufferedReader reader = new BufferedReader(fileReader);
        String line = "";
        while ((line = reader.readLine()) != null) {
            String[] data = line.split(",");
            if (data[0].trim().equals("Letter")){
                Letter room = new Letter(data[1].trim(), data[2].trim(),data[3].trim(),data[4].trim(),data[5].trim(),data[6].trim(),data[7].trim(),data[8].trim(),data[9].trim());
                supplies.addsupply(room);
            }
            else if (data[0].trim().equals("Package")){
                Letter room = new Package(data[1].trim(), data[2].trim(),data[3].trim(),data[4].trim(),data[5].trim(),data[6].trim(),data[7].trim(),data[8].trim(),data[9].trim(),data[10].trim(),data[11].trim());
                supplies.addsupply(room);
            }
            else if (data[0].trim().equals("Document")){
                Letter room = new Document(data[1].trim(), data[2].trim(),data[3].trim(),data[4].trim(),data[5].trim(),data[6].trim(),data[7].trim(),data[8].trim(),data[9].trim(),data[10].trim());
                supplies.addsupply(room);
            }
        }
        reader.close();
    }
    public ControllerClass Read(){
        try {
            ReadSupply();
        } catch (FileNotFoundException e) {
            System.err.println(this.fileName + " not found");
        } catch (IOException e) {
            System.err.println("IOException from reading " + this.fileName);
        }
        return supplies;
    }
    @Override
    public void setData(ControllerClass students) {
        String filePath = fileDirectoryName + File.separator + fileName;
        File file = new File(filePath);
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(file);
            BufferedWriter writer = new BufferedWriter(fileWriter);
            for (Letter student: students.toListSupply()) {
                if (student.getClass().equals(Letter.class)){
                    String line = "Letter" + ","
                            + student.getName() + ","
                            + student.getSurname() + ","
                            + student.getSentName() + ","
                            + student.getSentSurname()+ ","
                            +student.getAddress()+","
                            + String.valueOf(student.getSize())+","
                            +student.getPicture()+","
                            + student.getDay()+","
                            + student.getTime()+","
                            ;
                    writer.append(line);
                    writer.newLine();
                }
                else if (student.getClass().equals(Package.class)){
                    Package pack = (Package) student;
                    String line ="Package" + ","
                            + pack.getName() + ","
                            + pack.getSurname() + ","
                            + pack.getSentName() + ","
                            + pack.getSentSurname()+ ","
                            +pack.getAddress()+","
                            + String.valueOf(pack.getSize())+","
                            +pack.getDelivery()+","
                            +pack.getTrack()+","
                            +student.getPicture()+","
                            + student.getDay()+","
                            + student.getTime()+","
                            ;
                    writer.append(line);
                    writer.newLine();
                }
                else if (student.getClass().equals(Document.class)){
                    Document document = (Document) student;
                    String line = "Document" +","
                            + document.getName() + ","
                            + document.getSurname() + ","
                            + document.getSentName() + ","
                            + document.getSentSurname()+ ","
                            +document.getAddress()+","
                            + String.valueOf(document.getSize())+","
                            +document.getPriority()+","
                            +student.getPicture()+","
                            + student.getDay()+","
                            + student.getTime()+","
                            ;
                    writer.append(line);
                    writer.newLine();
                }
            }
            writer.close();
        } catch (IOException e) {
            System.err.println("Cannot write " + filePath);
        }
    }
}
