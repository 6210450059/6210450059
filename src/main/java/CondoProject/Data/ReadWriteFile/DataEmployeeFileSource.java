package CondoProject.Data.ReadWriteFile;

import CondoProject.Model.People.Customer;
import CondoProject.Model.People.Employee;
import CondoProject.Controller.ControllerClass;
import CondoProject.Data.DataB;

import java.io.*;

public class DataEmployeeFileSource implements DataB {
    private String fileDirectoryName;
    private String fileName;
    private ControllerClass employees;

    public DataEmployeeFileSource(String fileDirectoryName, String fileName) {
        this.fileDirectoryName = fileDirectoryName;
        this.fileName = fileName;
        checkFileIsExisted();

    }
    private void checkFileIsExisted() {
        File file = new File(fileDirectoryName);
        if (!file.exists()) {
            file.mkdirs();
        }
        String filePath = fileDirectoryName + File.separator + fileName;
        file = new File(filePath);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                System.err.println("Cannot create " + filePath);
            }
        }
    }
    private void readEmployee() throws IOException {
        String filePath = fileDirectoryName + File.separator + fileName;
        File file = new File(filePath);
        FileReader fileReader = new FileReader(file);
        BufferedReader reader = new BufferedReader(fileReader);
        String line = "";
        while ((line = reader.readLine()) != null) {
            String[] data = line.split(",");
            Employee employee = new Employee(data[1].trim(), data[2].trim(),data[3].trim(),data[4].trim(),data[5].trim(),data[6].trim());
            employees.addCustomer(employee);
        }
        reader.close();
    }
    public void setData(ControllerClass students) {
        String filePath = fileDirectoryName + File.separator + fileName;
        File file = new File(filePath);
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(file);
            BufferedWriter writer = new BufferedWriter(fileWriter);
            for (Customer student: students.toList()) {
                    Employee e1 = (Employee) student;
                    String line ="Employee" + ","
                            + e1.getName() + ","
                            + e1.getSurname() + ","
                            +e1.getId()+ ","
                            + e1.getPin() + ","
                            + e1.getDay()+","
                            +e1.getTime();
                    writer.append(line);
                    writer.newLine();

            }
            writer.close();
        } catch (IOException e) {
            System.err.println("Cannot write " + filePath);
        }
    }
    public ControllerClass Read(){
        try {
            employees = new ControllerClass();
            readEmployee();
        } catch (FileNotFoundException e) {
            System.err.println(this.fileName + " not found");
        } catch (IOException e) {
            System.err.println("IOException from reading " + this.fileName);
        }
        return employees;
    }
}
