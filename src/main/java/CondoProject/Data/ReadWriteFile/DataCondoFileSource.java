package CondoProject.Data.ReadWriteFile;

import CondoProject.Model.Condo;
import CondoProject.Controller.ControllerClass;
import CondoProject.Data.DataB;

import java.io.*;

public class DataCondoFileSource  implements DataB {
    private ControllerClass condo;
    private String fileDirectoryName;
    private String fileName;

    public DataCondoFileSource(String fileDirectoryName, String fileName) {
        this.fileDirectoryName = fileDirectoryName;
        this.fileName = fileName;
        condo = new ControllerClass();
        checkFileIsExisted();
    }

    private void checkFileIsExisted() {
        File file = new File(fileDirectoryName);
        if (!file.exists()) {
            file.mkdirs();
        }
        String filePath = fileDirectoryName + File.separator + fileName;
        file = new File(filePath);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                System.err.println("Cannot create " + filePath);
            }
        }
    }
    private void readDataCondo() throws IOException{
        String filePath = fileDirectoryName + File.separator + fileName;
        File file = new File(filePath);
        FileReader fileReader = new FileReader(file);
        BufferedReader reader = new BufferedReader(fileReader);
        String line = "";
        while ((line = reader.readLine()) != null) {
            String[] data = line.split(",");
            Condo room = new Condo(data[0].trim(), data[1].trim(),data[2].trim(),data[3].trim());
            condo.addRoom(room);
        }
        reader.close();
    }
    public ControllerClass Read(){
        try {
            readDataCondo();
        } catch (FileNotFoundException e) {
            System.err.println(this.fileName + " not found");
        } catch (IOException e) {
            System.err.println("IOException from reading " + this.fileName);
        }
        return condo;
    }
    public void setData(ControllerClass students) {
        String filePath = fileDirectoryName + File.separator + fileName;
        File file = new File(filePath);
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(file);
            BufferedWriter writer = new BufferedWriter(fileWriter);
            for (Condo student: students.toListCondo()) {
                String line = student.getBuilding() + ","
                        + student.getLayers() + ","
                        + student.getRoom() + ","
                        + student.getRoomType();
                writer.append(line);
                writer.newLine();
            }

            writer.close();
        } catch (IOException e) {
            System.err.println("Cannot write " + filePath);
        }
    }

}
