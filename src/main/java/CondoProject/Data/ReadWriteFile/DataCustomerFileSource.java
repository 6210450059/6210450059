package CondoProject.Data.ReadWriteFile;


import CondoProject.Model.People.Customer;
import CondoProject.Controller.ControllerClass;
import CondoProject.Data.DataB;

import java.io.*;


public class DataCustomerFileSource implements DataB {

    private String fileDirectoryName;
    private String fileName;
    private ControllerClass customers;

    public DataCustomerFileSource(String fileDirectoryName, String fileName) {
        this.fileDirectoryName = fileDirectoryName;
        this.fileName = fileName;
        checkFileIsExisted();
        customers = new ControllerClass();
    }

    private void checkFileIsExisted() {
        File file = new File(fileDirectoryName);
        if (!file.exists()) {
            file.mkdirs();
        }
        String filePath = fileDirectoryName + File.separator + fileName;
        file = new File(filePath);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                System.err.println("Cannot create " + filePath);
            }
        }
    }
    private void readCustomer() throws IOException {
        String filePath = fileDirectoryName + File.separator + fileName;
        File file = new File(filePath);
        FileReader fileReader = new FileReader(file);
        BufferedReader reader = new BufferedReader(fileReader);
        String line = "";
        while ((line = reader.readLine()) != null) {
            String[] data = line.split(",");
            Customer customer = new Customer(data[1].trim(), data[2].trim(),data[3].trim(),data[4].trim(),data[5].trim(),data[6].trim(),data[7].trim(),data[8].trim());
            customers.addCustomer(customer);
        }
        reader.close();
    }
    public ControllerClass Read(){
        try {
            readCustomer();
        } catch (FileNotFoundException e) {
            System.err.println(this.fileName + " not found");
        } catch (IOException e) {
            System.err.println("IOException from reading " + this.fileName);
        }
        return customers;
    }






    @Override
    public void setData(ControllerClass students) {
        String filePath = fileDirectoryName + File.separator + fileName;
        File file = new File(filePath);
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(file);
            BufferedWriter writer = new BufferedWriter(fileWriter);
            for (Customer student: students.toList()) {
                    Customer c1 =  student;
                    String line = "Customer" + ","
                            + c1.getName() + ","
                            + c1.getSurname() + ","
                            + c1.getId() + ","
                            + c1.getPin() + ","
                            + c1.getBuilding() + ","
                            + c1.getLayer() + ","
                            + c1.getRoom() + ","
                            + c1.getRoomType();
                    writer.append(line);
                    writer.newLine();
            }
            writer.close();
        } catch (IOException e) {
            System.err.println("Cannot write " + filePath);
        }
    }

}
