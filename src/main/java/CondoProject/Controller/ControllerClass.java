package CondoProject.Controller;

import CondoProject.Model.Condo;
import CondoProject.Model.People.Customer;
import CondoProject.Model.People.Employee;
import CondoProject.Controller.Compare.CompareClass;
import CondoProject.Controller.Compare.CompareCondo;
import CondoProject.Controller.Compare.CompareEmployee;
import CondoProject.Controller.Compare.CompareLetter;
import CondoProject.Model.supply.Letter;


import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;

import java.util.Date;


public class ControllerClass {

    private ArrayList<Customer> accountList; /**Deal with Data**/
    private ArrayList<Condo> condoArrayList;
    private ArrayList<Letter> controlSupplyClasses;
    private Customer data;



    public void setAccountList(ArrayList<Customer> accountList) {
        this.accountList = accountList;
    }
    public void addAccountList(ArrayList<Customer> accountList){
        this.accountList.addAll(accountList);
    }



    public void setControlSupplyClasses(ArrayList<Letter> controlSupplyClasses){
        this.controlSupplyClasses = controlSupplyClasses;
    }
    public void addsupply(Letter c){
        controlSupplyClasses.add(c);
    }


    public void setCondoArrayList(ArrayList<Condo> condoArrayList) {
        this.condoArrayList = condoArrayList;
    }

    public ControllerClass(){
        accountList = new ArrayList<>();
            condoArrayList = new ArrayList<>();
            controlSupplyClasses = new ArrayList<>();
    }

    public void addDataList(Customer cus){
        accountList.add(cus);
    } /**add data in to interfaces**/
    public void addRoom(Condo condo){condoArrayList.add(condo);}
    public void addCustomer(Customer customer){
        accountList.add(customer);
    }

    public boolean Check(String id,String pin) {
        for (Customer acc: accountList){
            if (acc.getId().equals(id) && acc.getPin().equals(pin)){
                data = acc;
                return  true;
            }
        }
        data = null;
        return false;
    }
    public Customer getClassType(){ /**Return class type**/
        if (data.getClass() == Customer.class){
            return (Customer) data;
        }
        else {
            Employee em = (Employee) data;
            return (Employee) data;
        }

    }
    public void settime(){
        for (Customer acc : accountList){
            if (acc.getClass() == Employee.class){
                if (acc.getPin().equals(data.getPin())&&acc.getId().equals(data.getId())){
                    LocalDateTime date = LocalDateTime.now();
                    DateTimeFormatter simpleDateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH:mm:ss");
                    String strdate = simpleDateFormat.format(date);
                    ((Employee) acc).setDay(strdate.substring(0,10));
                    ((Employee) acc).setTime(strdate.substring(11,19));
                }
            }
        }

    }


    public ArrayList<Customer> toListCustomer() {
        ArrayList<Customer> customer = new ArrayList<>();
        for (Customer acc: accountList){
            if (acc.getClass() == Customer.class){
                customer.add(acc);
            }
        }
        return customer;
    }
    public ArrayList<Customer> toListEmployee(){
        CompareClass compareEmployee = new CompareEmployee<Employee>();
        ArrayList<Customer> employees = new ArrayList<>();
        for (Customer acc:accountList){
            if (acc.getClass() == Employee.class){
                employees.add(acc);
            }
        }
        Collections.sort(employees,compareEmployee.reversed());
        return employees;
    }
    public ArrayList<Customer> toList(){
        return  accountList;
    }
    public ArrayList<Letter> toListSupply(){
        CompareClass  letterCompareLetter = new CompareLetter<Letter>();
        Collections.sort(controlSupplyClasses,letterCompareLetter);
        return controlSupplyClasses;}
    public ArrayList<Condo> toListCondo(){
        CompareClass  condoComparator = new CompareCondo<Condo>();
        Collections.sort(condoArrayList,condoComparator);
        ArrayList<Condo> condos = new ArrayList<>();
        for (Condo acc:condoArrayList){
            condos.add((Condo) acc);

        }
        return condos;
    }
    public ArrayList<Condo> toListCondoSeparate(String building){
        CompareClass  condoComparator = new CompareCondo<Condo>();
        Collections.sort(condoArrayList,condoComparator);
        ArrayList<Condo> condos = new ArrayList<>();
        for (Condo acc:condoArrayList){
            if (acc.getBuilding().equals(building)) condos.add(acc);
        }
        return condos;
    }


    public ArrayList<Customer> toListShowRoom(){
        ArrayList<Customer>  customerArrayList = new ArrayList<>();
        ArrayList<Customer> customers = new ArrayList<>();
        for (Condo condo : condoArrayList){
            if (condo.getRoomType().equals("DeluxeRoom")){
                customerArrayList.add(new Customer("","","","",condo.getBuilding(),condo.getLayers(),condo.getRoom(),condo.getRoomType()));
            }
            customerArrayList.add(new Customer("","","","",condo.getBuilding(),condo.getLayers(),condo.getRoom(),condo.getRoomType()));
        }
        for (Customer customer :accountList){
            if (customer.getClass().equals(Customer.class)) customers.add(customer);}
        for (Customer customer : customers){
            for (Customer c : customerArrayList){
                if (c.getRoom().equals(customer.getRoom()) && c.getBuilding().equals(customer.getBuilding())&&c.getName().equals("")){
                    c.setName(customer.getName());
                    c.setSurname(customer.getSurname());
                    break;

                }
            }
        }
        return  customerArrayList;
    }

    @Override
    public String toString() {
        return accountList.toString();
    }

}
