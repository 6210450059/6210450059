package CondoProject.Controller.Compare;

import CondoProject.Model.Condo;

import java.util.Comparator;

public class CompareCondo<T> implements Comparator<T>,CompareClass<T> {
    @Override
    public int compare(T o1, T o2) {
        Condo c1 = (Condo) o1;
        Condo c2 = (Condo) o2;
        if (c1.getBuilding().compareTo(c2.getBuilding()) == 0)
            return c1.getRoom().compareTo(c2.getRoom());
        else {
            return c1.getBuilding().compareTo(c2.getBuilding());
        }
    }
}
