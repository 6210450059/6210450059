package CondoProject.Controller.Compare;

import java.util.Comparator;

public interface CompareClass<T> extends Comparator<T> {
    int compare(T o1, T o2);
}
