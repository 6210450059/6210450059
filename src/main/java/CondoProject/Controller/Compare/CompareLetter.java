package CondoProject.Controller.Compare;

import CondoProject.Model.supply.Letter;

import java.util.Comparator;

public class CompareLetter<T> implements Comparator<T>,CompareClass<T> {
    @Override
    public int compare(T o1, T o2) {
        Letter l1 = (Letter)o1;
        Letter l2 = (Letter)o2;
        if (l1.getDay().compareTo(l2.getDay()) < 0)
            return l1.getTime().compareTo(l2.getTime());
        else {
            return l1.getDay().compareTo(l2.getDay());
        }
    }
}
