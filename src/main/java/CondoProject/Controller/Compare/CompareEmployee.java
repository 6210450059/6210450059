package CondoProject.Controller.Compare;

import CondoProject.Model.People.Employee;

import java.util.Comparator;

public class CompareEmployee<T> implements Comparator<T>,CompareClass<T>  {
    @Override
    public int compare(T o1, T o2) {
        Employee l1 = (Employee)o1;
        Employee l2 = (Employee)o2;
        if (l1.getDay().compareTo(l2.getDay()) == 0)
            return l1.getTime().compareTo(l2.getTime());
        else {
            return l1.getDay().compareTo(l2.getDay());
        }
}
}
