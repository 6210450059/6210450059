-Features ที่นำมาพัฒนา - 13.3 และ 13.1(ยังไม่สมบูรณ์)
-week 1 -สร้างโปรเจค , เพิ่ม Maven และสร้าง Class และ Interface
-week 2 -Update หน้า Register สร้าง DataHardcode และ polymorphism
-week 3 -สร้าง DataSourceFile จาก .csv และสร้างหน้า Table สำหรับดูผู้ใช้ของ พนักงานส่วนกลาง
-week 4 -update หน้า Register(เสร็จ) และ update หน้า Table และสร้าง Readme
-Week 5 
	commit 7 -Update หน้า ตารางลูกค้า และ สร้างหน้าสำหรับแก้ไขห้อง(ยังไม่ได้ทำ)
	commit 8 - สร้างหน้าจัดการสินค้า(ยังไม่เสร็จ)
	commit 9 -  อัพเดทหน้าจัดการสินค้า และ แก้ไขหน้าตารางลูกค้า(ยังไม่เสร็จ)
	commit 10 - อัพเดทหน้าตารางลูกค้า สร้างหน้าสำหรับ admin และ สร้างหน้า สมัครพนักงาน
	commit 11 - อัพเดทหน้าสำหรับแก้ไขข้อมูล 
	commit 12 -  เพิ่ม ไฟล์ pdf
Structure
	- Condo -> รวม class ของ ผู้ใช้ พนักงาน และเอกสาร
	- Controller -> เป็นตัวควบคุม เช่นการแยก คลาสระหว่าง Employee และ Customer เป็นตัวเก็บข้อมูลของ SourceDataและHardFile เป็นต้น
	- CustomerHP(CustomerHomepage) -> เป็นหน้าของผู้ใช้บริการ
	- EmployeeHP(EmployeeHomepage) -> เป็นหน้าของผู้ใช้ส่วนกลาง
		-informationcustomer -> หน้าจัดการสำหรับลูกค้า -> ตารางลูกค้า การแก้ไขห้อง
		-supplyhandle -> เป็นหน้าสำหรับการจัดการพัสดุเอกสาร -> การแสดงพัสดุ การนำพัสดุเข้าระบบ
	- RegisterHP(Registerhomepage) -> เป็นหน้าของการสมัครทั้ง พนักงานและ ผู้ใช้บริการ
	- Data -> จัดการสำหรับเรื่องการ อ่านไฟล์ เขียนไฟล์
	- Info -> หน้าสำหรับผู้จัดทำและ ข้อแนะนำ


โดย jarfile จะอยู่ใน folder CondoProject
วิธีการใช้งาน
	สำหรับเจ้าหน้าที่ส่วนกลาง Username: 0000 Password: 0000
	สำหรับ admin Username: admin Password: admin
 กรณีที่เข้าไม่ได้ให้ลอง
เข้าไปที่ Folder แล้วเปิด CMD พิม java -jar 6210450059-jar.jar
-------------------------------------------------------------------------------------
 "ส่งงานแก้ไขโปรเจครอบที่ 2 วิชา 01418211"
Feature ที่แก้ไข/เพิ่มเติม
	- สามารถเพิ่มห้องพักได้
	- มีการเรียงตามวันที่ทั้ง สินค้าที่เพิ่ม และ staff
	- ไม่ได้แก้ไข ข้อ(16) login customer (ข้อขาว)
	-  นำ condo.csv มารวมกันและแยก ไฟล์ของ customer และ employee ออกจากกัน
	-  สามารถตั้งรหัสซ้ำกันได้
	-  แยก table column building ออกเป็นหลาย column
	-  แก้ไขเมื่อเพิ่มผู้เข้าพักเสร็จแล้ว ออกไปยังหน้า login
-------------------------------------------------------------------------------------
 "ส่งงานแก้ไขโปรเจครอบที่ 3 วิชา 01418211"
 Feature ที่แก้ไข/เพิ่มเติม
	- แก้ไขการเรียง สินค้าและ staff